#!/bin/sh -e

cat /etc/alertmanager/config.yml | \
    sed "s@#webhook_url: 'null'#@url: '$WEBHOOK_URL'@g" > /tmp/config.yml

mv /tmp/config.yml /etc/alertmanager/config.yml

set -- $ALERTMANAGER_BIN "$@"

exec "$@"
